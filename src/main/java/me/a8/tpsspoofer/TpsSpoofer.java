package me.a8.tpsspoofer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public final class TpsSpoofer extends JavaPlugin {

    @Override
    public void onEnable() {
        int pluginId = 7205;
        Metrics metrics = new Metrics(this, pluginId);

        System.out.println(ChatColor.GREEN + "TpsSpoofer by a8_");
        Bukkit.getPluginManager().registerEvents((Listener)new TpsSpoof(), (Plugin)this);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
