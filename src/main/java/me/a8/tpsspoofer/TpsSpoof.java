package me.a8.tpsspoofer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.Random;

public class TpsSpoof implements Listener {
    @EventHandler(priority = EventPriority.LOWEST)
    public void onCommandPreProcess(PlayerCommandPreprocessEvent event) {
        String[] cmd = event.getMessage().split(" ");
        if (cmd[0].equals("/tps")) {

            event.setCancelled(true);
            Player player = event.getPlayer();
            if(player.hasPermission("tpsspoof.use")) {

                if (Bukkit.getServer().getOnlinePlayers().size() >= 100) {
                    Random rand = new Random();
                    int n = rand.nextInt(2);
                    n += 1;
                    if (n == 1) {
                        player.sendMessage(ChatColor.GOLD + "TPS from last 1m, 5m, 15m:" + ChatColor.GREEN + " 20.0, 20.0, 20.0");
                    } else {
                        player.sendMessage(ChatColor.GOLD + "TPS from last 1m, 5m, 15m:" + ChatColor.GREEN + " 19.99, 20.0, 20.0");
                    }
                } else {
                    player.sendMessage(ChatColor.GOLD + "TPS from last 1m, 5m, 15m:" + ChatColor.GREEN + " 20.0, 20.0, 20.0");
                }

            }else{
                player.sendMessage(ChatColor.RED + "I'm sorry, but you do not have permission to preform this command. Please contact the server admistrators if you beleve that this is in error.");
            }

        }
    }

}
